# TP Chenillard

> Petit TP Arduino permettant d'allumer des leds les unes après les autres avec transition aller-retour.

- [x] Codes sources Arduino (.ino)
- [x] Fichiers sources schéma électrique (Proteus)
---
- [ ] Codes Arduino commentés
---

<img src = "https://gitlab.com/maxoulfou/tp-chenillard/raw/master/Images/Chenillard.PNG" title = "schéma" alt = "Chenillard Arduino Schema">

© Maxence Brochier 2018 - 2019