int pinLed[] = {9, 10, 11, 12, 13};

void setup()
{
  for (int i = 0; i < 5; i++)
  {
    pinMode(pinLed[i], OUTPUT);
    digitalWrite(pinLed[i], LOW);
  }
}

void loop()
{
  for (int temps = 1000; temps >= 200; temps -= 400)
  {
    for (int i = 0; i < 5; i++)
    {
      digitalWrite(pinLed[i], HIGH);
      delay(temps);
      digitalWrite(pinLed[i], LOW);
      delay(1);
    }
    for (int i = 3; i > 0; i--)
    {
      digitalWrite(pinLed[i], HIGH);
      delay(temps);
      digitalWrite(pinLed[i], LOW);
      delay(1);
    }
  }
}
